<?php
/**
 * @file
 * personal_wishlist.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function personal_wishlist_user_default_permissions() {
  $permissions = array();

  // Exported permission: create wishlist content.
  $permissions['create wishlist content'] = array(
    'name' => 'create wishlist content',
    'roles' => array(),
  );

  // Exported permission: delete any wishlist content.
  $permissions['delete any wishlist content'] = array(
    'name' => 'delete any wishlist content',
    'roles' => array(),
  );

  // Exported permission: delete own wishlist content.
  $permissions['delete own wishlist content'] = array(
    'name' => 'delete own wishlist content',
    'roles' => array(),
  );

  // Exported permission: edit any wishlist content.
  $permissions['edit any wishlist content'] = array(
    'name' => 'edit any wishlist content',
    'roles' => array(),
  );

  // Exported permission: edit own wishlist content.
  $permissions['edit own wishlist content'] = array(
    'name' => 'edit own wishlist content',
    'roles' => array(),
  );

  return $permissions;
}
