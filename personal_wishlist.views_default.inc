<?php
/**
 * @file
 * personal_wishlist.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function personal_wishlist_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'wishlist';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Wishlist';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Wishlist: Stuff I Wished I Had';
  $handler->display->display_options['css_class'] = 'wishlist_view';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['grouping'] = 'field_wishlist_status';
  $handler->display->display_options['style_options']['columns'] = '3';
  $handler->display->display_options['style_options']['fill_single_line'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<br />
<p>This is my wishlist, where I keep adding stuff I wish I had, just so that I remember them when I go out shopping. This list also serves the purpose of helping out my friends and family decide, what to get me, whenever they feel like getting me something ;)</p>
<br />
';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = '<hr />
<p>Indeed, I am very lucky that I have everything I ever wished for. Can you beat that?</p>
<p>Oh wait, how about if I can have this shiny thingy they are talking about releasing this weekend. Oh If only I could lay my hands on it. Seriously, we wants it.</p>
<p>We Wants It!</p>
<p>We wants to hold it in our hands. We wants to keep it away from those filthy little hobitses. <strong>My Precioussss!</strong></p>
<p><img alt="Gollum" height="400px" src="http://fc03.deviantart.net/fs5/i/2004/345/3/c/Gollum_by_CurseReaper.jpg" /></p>
';
  /* Field: Content: Box Art */
  $handler->display->display_options['fields']['field_wishlist_box']['id'] = 'field_wishlist_box';
  $handler->display->display_options['fields']['field_wishlist_box']['table'] = 'field_data_field_wishlist_box';
  $handler->display->display_options['fields']['field_wishlist_box']['field'] = 'field_wishlist_box';
  $handler->display->display_options['fields']['field_wishlist_box']['label'] = '';
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_wishlist_box']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_wishlist_box']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_wishlist_box']['settings'] = array(
    'image_style' => 'wishlist_box',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_wishlist_box']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_type'] = 'h4';
  $handler->display->display_options['fields']['title']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_wishlist_category']['id'] = 'field_wishlist_category';
  $handler->display->display_options['fields']['field_wishlist_category']['table'] = 'field_data_field_wishlist_category';
  $handler->display->display_options['fields']['field_wishlist_category']['field'] = 'field_wishlist_category';
  $handler->display->display_options['fields']['field_wishlist_category']['label'] = '';
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['text'] = '([field_wishlist_category])';
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['element_type'] = 'strong';
  $handler->display->display_options['fields']['field_wishlist_category']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_wishlist_category']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_wishlist_category']['click_sort_column'] = 'tid';
  $handler->display->display_options['fields']['field_wishlist_category']['field_api_classes'] = 0;
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_wishlist_status']['id'] = 'field_wishlist_status';
  $handler->display->display_options['fields']['field_wishlist_status']['table'] = 'field_data_field_wishlist_status';
  $handler->display->display_options['fields']['field_wishlist_status']['field'] = 'field_wishlist_status';
  $handler->display->display_options['fields']['field_wishlist_status']['label'] = '';
  $handler->display->display_options['fields']['field_wishlist_status']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['element_type'] = 'strong';
  $handler->display->display_options['fields']['field_wishlist_status']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_wishlist_status']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_wishlist_status']['field_api_classes'] = 0;
  /* Field: Content: Price */
  $handler->display->display_options['fields']['field_wishlist_price']['id'] = 'field_wishlist_price';
  $handler->display->display_options['fields']['field_wishlist_price']['table'] = 'field_data_field_wishlist_price';
  $handler->display->display_options['fields']['field_wishlist_price']['field'] = 'field_wishlist_price';
  $handler->display->display_options['fields']['field_wishlist_price']['label'] = '';
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['text'] = '[field_wishlist_price]';
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['element_type'] = 'em';
  $handler->display->display_options['fields']['field_wishlist_price']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_wishlist_price']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_wishlist_price']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_wishlist_price']['field_api_classes'] = 0;
  /* Sort criterion: Fields: Status (field_wishlist_status) */
  $handler->display->display_options['sorts']['field_wishlist_status_value']['id'] = 'field_wishlist_status_value';
  $handler->display->display_options['sorts']['field_wishlist_status_value']['table'] = 'field_data_field_wishlist_status';
  $handler->display->display_options['sorts']['field_wishlist_status_value']['field'] = 'field_wishlist_status_value';
  /* Sort criterion: Fields: Category (field_wishlist_category) - tid */
  $handler->display->display_options['sorts']['field_wishlist_category_tid']['id'] = 'field_wishlist_category_tid';
  $handler->display->display_options['sorts']['field_wishlist_category_tid']['table'] = 'field_data_field_wishlist_category';
  $handler->display->display_options['sorts']['field_wishlist_category_tid']['field'] = 'field_wishlist_category_tid';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'wishlist' => 'wishlist',
  );
  /* Filter criterion: Fields: Status (field_wishlist_status) */
  $handler->display->display_options['filters']['field_wishlist_status_value']['id'] = 'field_wishlist_status_value';
  $handler->display->display_options['filters']['field_wishlist_status_value']['table'] = 'field_data_field_wishlist_status';
  $handler->display->display_options['filters']['field_wishlist_status_value']['field'] = 'field_wishlist_status_value';
  $handler->display->display_options['filters']['field_wishlist_status_value']['value'] = array(
    'must' => 'must',
    'good' => 'good',
    'wait' => 'wait',
    'why' => 'why',
  );
  $handler->display->display_options['filters']['field_wishlist_status_value']['expose']['operator_id'] = 'field_wishlist_status_value_op';
  $handler->display->display_options['filters']['field_wishlist_status_value']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['field_wishlist_status_value']['expose']['operator'] = 'field_wishlist_status_value_op';
  $handler->display->display_options['filters']['field_wishlist_status_value']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['field_wishlist_status_value']['expose']['reduce'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'wishlist';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Wishlist';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $translatables['wishlist'] = array(
    t('Master'),
    t('Wishlist: Stuff I Wished I Had'),
    t('more'),
    t('Filter'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('<br />
<p>This is my wishlist, where I keep adding stuff I wish I had, just so that I remember them when I go out shopping. This list also serves the purpose of helping out my friends and family decide, what to get me, whenever they feel like getting me something ;)</p>
<br />
'),
    t('<hr />
<p>Indeed, I am very lucky that I have everything I ever wished for. Can you beat that?</p>
<p>Oh wait, how about if I can have this shiny thingy they are talking about releasing this weekend. Oh If only I could lay my hands on it. Seriously, we wants it.</p>
<p>We Wants It!</p>
<p>We wants to hold it in our hands. We wants to keep it away from those filthy little hobitses. <strong>My Precioussss!</strong></p>
<p><img alt="Gollum" height="400px" src="http://fc03.deviantart.net/fs5/i/2004/345/3/c/Gollum_by_CurseReaper.jpg" /></p>
'),
    t('([field_wishlist_category])'),
    t('[field_wishlist_price]'),
    t('Status'),
    t('Page'),
  );
  $export['wishlist'] = $view;

  return $export;
}
