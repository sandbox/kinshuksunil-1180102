PERSONAL WISHLIST MODULE
Author: Kinshuk Sunil

********************************************************************************
The Personal Wishlist module is a feature package exported through the Features
module. It was first created by me for my personal consumption on my website, 
but then was published here as a usable community module.

The module is meant for personal websites, where the site owner wants to list a 
personal wishlist. The owner (and other users with privileges) can create nodes 
of type 'wishlist item' which includes the name, box art, banner image, price, 
more details link, item category, level of desire and description. Ideal for 
letting your grandparents know what you want for Christmas.

Unlike other wishlist modules, this module is for Drupal 7 only.

********************************************************************************
The module WILL ADD the following:
1 Node Type: Wishlist Item, these nodes will be displayed at /wishlist. 
The node type has fields for Product Type (Taxonomy), More Details (URL), 
Price (Text), Title, Description, Box Art and Banner Image, Your level of
desperation for the product.

1 View, with a Page display, which is used at /wishlist to display the list. It 
currently lists all your wishlist items, grouped by your level of desperation.

2 Image styles: 1 each for wishlist item's box art and in-page banner.

1 Link item in the Main Menu

1 Taxonomy called Product Type 

********************************************************************************
DIFFERENCE FROM OTHER WISHLIST MODULES

This module is for Drupal 7

Meant for Personal websites or niche user websites (example: Family, very 
specific communities)

Simple listing of wishlist, no ecommerce involved

Easy to maintain: you just keep creating nodes

Extensible: since this theme works with basic drupal blocks like nodes, views 
and taxonomies, this module is very easy to build over and add workflows and 
functionalities

********************************************************************************
INSTALLATION

Upload an enable the module, and you should be good to go.

********************************************************************************
CONTACT

I do not commit to help you by providing support.

d.o: http://drupal.org/user/62196

email: kinshuksunil at gmail dot com

#drupal-support: Kinshuk

********************************************************************************
