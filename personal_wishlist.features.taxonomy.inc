<?php
/**
 * @file
 * personal_wishlist.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function personal_wishlist_taxonomy_default_vocabularies() {
  return array(
    'product_type' => array(
      'name' => 'Product Type',
      'machine_name' => 'product_type',
      'description' => 'Used as category in wishlist',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
