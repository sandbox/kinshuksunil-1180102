<?php
/**
 * @file
 * personal_wishlist.features.inc
 */

/**
 * Implements hook_views_api().
 */
function personal_wishlist_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => 3.0);
  }
}

/**
 * Implements hook_image_default_styles().
 */
function personal_wishlist_image_default_styles() {
  $styles = array();

  // Exported image style: wishlist_banner
  $styles['wishlist_banner'] = array(
    'name' => 'wishlist_banner',
    'effects' => array(
      6 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '494',
          'height' => '250',
        ),
        'weight' => '1',
      ),
      7 => array(
        'label' => 'Desaturate',
        'help' => 'Desaturate converts an image to grayscale.',
        'effect callback' => 'image_desaturate_effect',
        'module' => 'image',
        'name' => 'image_desaturate',
        'data' => array(),
        'weight' => '2',
      ),
    ),
  );

  // Exported image style: wishlist_box
  $styles['wishlist_box'] = array(
    'name' => 'wishlist_box',
    'effects' => array(
      8 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '120',
          'height' => '180',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function personal_wishlist_node_info() {
  $items = array(
    'wishlist' => array(
      'name' => t('Wishlist Item'),
      'base' => 'node_content',
      'description' => t('An item in my Wishlist - things I have to have. You can probably use this list to gift me stuff.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
